// console.log("Hello world")
// [Section] JavaScript Synchronous and Asynchronous
// javascript is by default synchronous

console.log("Hello world")
// conosole.log("hello again")
console.log("goodbye")

// code blocking

// async means that we can proceed to execute otehr statements, while time consuming codes are running in the background

// the fecth API allows you to async request for resource (data)
// a 'promise' is an object that represents the eventual completion (or failure) of an async function and its resulting value
/* 
    fetch('URL')
    .then((response)=>{})
*/
// [CHECK THE STATUS OF THE REQUEST]
// by using the .then method, we can now check for the status of the promise
fetch('https://jsonplaceholder.typicode.com/posts')
.then((response)=>console.log(response.status))
// the 'fetch' method will return a promise that resolves to a 'response' object
// the .then method captures the 'response' obj and returns another 'promise' which will eventually be 'resolved' or 'rejected'

// [RETRIEVE CONTENTS/DATA FROM THE RESPONSE OBJECT]
fetch('https://jsonplaceholder.typicode.com/posts')
.then((response)=>response.json())
// using multiple .then method creates a promise chain
.then((json)=>console.log(json))

// [CREATE A FUNCTION THAT WILL DEMONSTRATE USING "ASYNC" AND 'AWAIT' KEYWORDS]
// the async and await keywords is another approach is another approach that can be used to achieve an async code hahaa
// creates an async function

async function fetchData() {

    // waits for the fetch method to complete then stores the value in the result variable
    let result = await fetch('https://jsonplaceholder.typicode.com/posts')
    console.log(result);

    // result returned by fetch is actually a "promise"
    console.log(result);

    // The returned response is an objeect
    console.log(typeof result);

    // We can not access the content of the response directly by accessin its body property
    console.log(result.body);

    let json = await result.json();
    console.log(json);
}
fetchData();

// [RETRIEVE SPECIFIC POST]
    fetch('https://jsonplaceholder.typicode.com/posts/1')
    .then((response)=>response.json)
    .then((json)=>console.log(json))


// [CREATE POST]
/* 
    syntax:
    fetch('url',options)
    .then((response)=>{})
    .then((response)=>{})
*/

    fetch('https://jsonplaceholder.typicode.com/posts',{
        // sets the method of the req obj to post
        method:'POST',
        // specified that the content will be in a JSON structure
        header: {'Content-Type':'application/json'},
        body: JSON.stringify({title:'New Post',body:'Hello World!',userID:1})
    })
.then ((response)=>response.json())
.then((json)=>console.log(json));


fetch('https://jsonplaceholder.typicode.com/posts/1',{
    method:'PUT',
    header:{
        'Content-type':'application/json',
    }, 
    body: JSON.stringify({
        id:1,
        title: 'Updated post',
        body: "Hello again!",
        userId: 1
    })
})
.then((response)=>response.json())
.then((json)=> console.log(json));

// [UPDATE POST USING PATCH METHOD]
// PUT is used to update the whole object
// PATCH is used to update the single property

fetch('https://jsonplaceholder.typicode.com/posts/1',{
    method:'PATCH',
    header:{'Content-Type':'application/json'},
    body: JSON.stringify({title: 'Corrected Post'})
})
.then((response)=>response.json())
.then((json)=> console.log(json));


// [DELETE A POST]
    fetch('https://jsonplaceholder.typicode.com/posts/1', {
        method: 'DELETE'
    })